$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
    	interval: 3000
    });

    $('#contacto').on('show.bs.modal', function (e){
    	console.log('El modal se está mostrando');
        $('#contactobtn').removeClass('btn-success');
    	$('#contactobtn').addClass('btn-outline-success');
    	$('#contactobtn').prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function (e){
    	console.log('El modal se mostró');
    });

    $('#contacto').on('hide.bs.modal', function (e){
    	console.log('El modal se está ocultando');
		$('#contactobtn').prop('disabled', false);
		$('#contactobtn').removeClass('btn-outline-success');
		$('#contactobtn').addClass('btn-success');
	});

    $('#contacto').on('hidden.bs.modal', function (e){
    	console.log('El modal se ocultó');
    });
});